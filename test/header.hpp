#ifndef HEADER_HPP_INCLUDED
#define HEADER_HPP_INCLUDED
class MyClass
{
    public:
    MyClass() = default;
    void update_a();
    private:
    int a_{};
};
#endif
