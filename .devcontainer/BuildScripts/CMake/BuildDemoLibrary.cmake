# Convenience function to be called from the main CMakeLists.txt
function (builddemoproject)

    # Library dependencies: headers
    add_subdirectory(${PROJECT_SOURCE_DIR}/structured_bindings)
    # add_subdirectory(${PROJECT_SOURCE_DIR}/test)

endfunction ()
