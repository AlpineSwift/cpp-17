#ifndef HEADER_HPP_INCLUDED
#define HEADER_HPP_INCLUDED
#include <type_traits>
template < typename T > class GeneralClass
{
   public:
    explicit GeneralClass ( int a ) : a_ ( a )
    {
        // do not allow instantiation for T = float
        static_assert ( ! std::is_floating_point< T >::value,
                        "GeneralClass must not be instantiated for floats." );
    };

   protected:
   private:
    const int a_;
};

#endif
